package com.example.guitarsfakeapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuitarsFakeApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuitarsFakeApiApplication.class, args);
    }

}
