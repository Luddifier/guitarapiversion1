package com.example.guitarsfakeapi;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Repository
public class GuitarRepository implements GuitarInterface {

    public Map<Integer, Guitars> guitars;
    {
        guitars = new HashMap<>();
        guitars.put(1, new Guitars (1, "Gibson", "Les Mats"));
        guitars.put(2, new Guitars(2, "Satriani", "Eastwood"));

    }

    //private  List<Guitars> guitars = seedGuitars();

/*    private HashMap <Integer, Guitars> guitars = new HashMap<>();

    guitars.put(1, new Guitars (1, "Gibson", "Les Mats"));
        guitars.put(2, new Guitars(2, "Satriani", "Eastwood"));
        return guitars;*/


/*
    private HashMap<Integer, Guitars> seedGuitars() {
        guitars.put(1, new Guitars (1, "Gibson", "Les Mats"));
        guitars.put(2, new Guitars(2, "Satriani", "Eastwood"));
        return guitars;
    }
*/




/*    private static List<Guitars> seedGuitars() {
        var guitars = new ArrayList<Guitars>();
        guitars.add(new Guitars(1, "Fender", "Telecaster"));
        guitars.add(new Guitars(2, "Jackson", "Kelly"));

        return guitars;
    }*/

    @Override
    public HashMap<Integer, Guitars>  getAllGuitars() {
        return (HashMap<Integer, Guitars>) guitars;
    }

    @Override
    public Guitars getGuitar(int id) {
        return guitars.get(id);


    }

    @Override
    public Guitars createGuitar(int id, Guitars guitar) {
        guitars.put(id, guitar);
        return getGuitar(guitar.getId());
    }

    @Override
    public Guitars replaceGuitar(int id, Guitars guitar) {
        var guitarToDelete = getGuitar(guitar.getId());
        guitars.replace(id, guitar);
        return getGuitar(guitar.getId());

    }

    @Override
    public Guitars modifyGuitar(Guitars guitar) {
        var guitarToModify = getGuitar(guitar.getId());
        guitarToModify.setModel(guitar.getModel());
        guitarToModify.setBrand(guitar.getBrand());

        return guitar;

    }


    @Override
    public GuitarShortDTO getShorty(int id) {
        var guitar = getGuitar(id);
        return GuitarDtoMapper.mapGuitarShortDto(guitar);

    }

    @Override
    public Map listByBrands() {


        Map<String, Integer> resultMap = new HashMap<String, Integer>();

        for (int key : guitars.keySet()) {
            String userName = guitars.get(key).getBrand();

            if (resultMap.containsKey(userName)) {
                resultMap.put(userName, resultMap.get(userName) + 1);
            } else {
                resultMap.put(userName, 1);
            }
        }

        return resultMap;

    }





    public boolean guitarExists(int id) {
        return guitars.containsKey(id);
    }

    public boolean isValidGuitar(Guitars guitar) {
        return guitar.getId() > 0 && guitar.getBrand() != null && guitar.getModel() != null;
    }

    public boolean isValidGuitar(Guitars guitar, int id) {
        return isValidGuitar(guitar) && guitar.getId() == id;
    }

}
