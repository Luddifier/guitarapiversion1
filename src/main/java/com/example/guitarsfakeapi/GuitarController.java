package com.example.guitarsfakeapi;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/guitars")
public class GuitarController {
    //Why do I need to instantiate the repository here? Is this a delegation of responsibility?
    private GuitarRepository guitarRepository;

    @Autowired
    public GuitarController(GuitarRepository guitarRepository) {this.guitarRepository = guitarRepository;}

    @GetMapping("")
    public ResponseEntity<HashMap> getAllGuitars() {
        return new ResponseEntity<>(guitarRepository.getAllGuitars(), HttpStatus.OK);
    }

    @GetMapping("/{value}")
    public ResponseEntity<Guitars> getGuitar(@PathVariable int value) {
        if (!guitarRepository.guitarExists(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

            return new ResponseEntity<>(guitarRepository.getGuitar(value), HttpStatus.OK);

        }

    @PostMapping
    public ResponseEntity<Guitars> createGuitar(@RequestBody Guitars guitar) {
        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        int value = guitar.getId();

        return new ResponseEntity<>(guitarRepository.createGuitar(value, guitar), HttpStatus.OK);

    }

    @PutMapping("/{value}")
    public ResponseEntity<Guitars> replaceGuitar(@PathVariable int value, @RequestBody Guitars guitar) {
        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!guitarRepository.guitarExists(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(guitarRepository.replaceGuitar(value, guitar),HttpStatus.OK);
    }

    @PatchMapping ("/{value}")
    public ResponseEntity<Guitars> modifyGuitar(@PathVariable int value, @RequestBody Guitars guitar) {

        if (!guitarRepository.isValidGuitar(guitar)) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if (!guitarRepository.guitarExists(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(guitarRepository.modifyGuitar(guitar),HttpStatus.OK);

    }

    @GetMapping("/short/{value}")
    public ResponseEntity<GuitarShortDTO> getShortGuitar(@PathVariable int value) {
        if (!guitarRepository.guitarExists(value)) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(guitarRepository.getShorty(value), HttpStatus.OK);
    }

    @GetMapping("/v1/api/guitars/brands")
    public ResponseEntity<Map> getSortByBrand() {
        return new ResponseEntity<>(guitarRepository.listByBrands(), HttpStatus.OK);
    }










    }


