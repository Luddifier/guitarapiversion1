package com.example.guitarsfakeapi;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class GuitarDtoMapper {

    public static GuitarShortDTO mapGuitarShortDto(Guitars guitar) {
        var shortGuitar = new GuitarShortDTO(guitar.getModel().toUpperCase());
        return shortGuitar;

    }

/*    public HashMap listGuitarsByBrands() {

        Map<String, Integer> resultMap = new HashMap<String, Integer>();

        for (int key : GuitarRepository.guitars.keySet()) {
            String userName = guitars.get(key).getBrand();

            if (resultMap.containsKey(userName)) {
                resultMap.put(userName, resultMap.get(userName) + 1);
            } else {
                resultMap.put(userName, 1);
            }
        }

        return resultMap;
        */
}
