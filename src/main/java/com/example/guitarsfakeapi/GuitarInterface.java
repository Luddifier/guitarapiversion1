package com.example.guitarsfakeapi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface GuitarInterface {

    HashMap<Integer, Guitars> getAllGuitars();

    Guitars getGuitar(int id);
    Guitars createGuitar(int id, Guitars guitar);
    Guitars replaceGuitar(int id, Guitars guitar);
    Guitars modifyGuitar(Guitars guitar);

    GuitarShortDTO getShorty(int id);
    Map listByBrands();
    //GuitarShortDTO mapGuitarShortDto(Guitars guitar);
}
